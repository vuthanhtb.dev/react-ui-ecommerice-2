import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Routes } from 'react-router-dom';
import { FooterComponent, HeaderComponent } from 'components';
import { AccountPage, HomePage, LoginPage, RegisterPage } from 'pages';

const App = () => {
  const isLogIn = useSelector((state) => state.auth.isLogIn);

  return (
    <div className="app">
      {
        isLogIn && (
          <>
            <HeaderComponent />
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/register" element={<RegisterPage />} />
              <Route path="/account" element={<AccountPage />} />
            </Routes>
            <FooterComponent />
          </>
        )
      }
      {
        !isLogIn && (<LoginPage />)
      }
    </div>
  );
};

export default App;
