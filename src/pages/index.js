export { default as AccountPage } from './account';
export { default as DetailPage } from './detail';
export { default as HomePage } from './home';
export { default as LoginPage } from './login';
export { default as RegisterPage } from './register';
