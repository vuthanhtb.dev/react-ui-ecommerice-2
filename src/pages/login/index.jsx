import React from 'react';
import { useDispatch } from 'react-redux';
import { authActions } from 'store/authSlice';
import back from 'assets/images/my-account.jpg';
import './index.css';

const Login = () => {
  const dispatch = useDispatch();

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(authActions.login());
  };

  return (
    <section className="login">
      <div className="container">
        <div className="backImg">
          <img src={back} alt="back" />
          <div className="text">
            <h3>Login</h3>
            <h1>My ACcount</h1>
          </div>
        </div>

        <form onSubmit={handleSubmit}>
          <span>Username or Email address</span>
          <input type="text" required />
          <span>Password * </span>
          <input type="password" required />
          <button className="button">Log in </button>
        </form>
      </div>
    </section>
  );
};

export default Login;
