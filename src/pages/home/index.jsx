import React from 'react';
import {
  CategoryComponent,
  OrderComponent,
  ProductComponent,
  SliderComponent
} from 'components';

const Home = () => {
  return (
    <>
      <SliderComponent />
      <OrderComponent />
      <CategoryComponent />
      <ProductComponent />
    </>
  );
};

export default Home;
