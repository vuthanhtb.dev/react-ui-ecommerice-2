import React from 'react';
import back from 'assets/images/my-account.jpg';
import './index.css';

const Register = () => {
  return (
    <section className="register">
      <div className="container">
        <div className="backImg">
          <img src={back} alt="" />
          <div className="text">
            <h3>Register</h3>
            <h1>My ACcount</h1>
          </div>
        </div>

        <form>
          <span>Email address</span>
          <input type="text" required />
          <span>Username * </span>
          <input type="text" required />
          <span>Password * </span>
          <input type="text" required />
          <span>Confirm Password * </span>
          <input type="text" required />
          <button className="button">Register</button>
        </form>
      </div>
    </section>
  );
};

export default Register;
