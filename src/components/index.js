export { default as CategoryComponent } from './category';
export { default as FooterComponent } from './footer';
export { default as HeaderComponent } from './header';
export { default as SliderComponent } from './slider';
export { default as OrderComponent } from './order';
export { default as ProductComponent } from './product';
export { default as ScrollTopComponent } from './scrollTop';
