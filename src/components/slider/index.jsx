import React from 'react';
import { slide } from 'assets/data/data';
import './index.css';

const Slider = () => {
  return (
    <div className="slider">
      <div className="container grid">
        {slide.map((item, i) => (
          <div className="box" key={i}>
            <div className="img">
              <img src={item.image} alt="img" />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Slider;
