import React from 'react';
import { product } from 'assets/data/data';
import ProductCart from './productCart';
import './index.css';

const Product = () => {
  return (
    <section className="product">
      <div className="container grid3">
        {product.map((item, index) => (
          <ProductCart
            key={index}
            id={item.id}
            cover={item.cover}
            name={item.name}
            price={item.price}
          />
        ))}
      </div>
    </section>
  );
};

export default Product;
